<!-- Color palette: http://paletton.com/export/index.php -->
<html>

<head>

<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->

<link rel="stylesheet" href="styles.css">
</head>

<body>

<?php 

$line = '';

$f = fopen('status.txt', 'r');
$cursor = -1;

fseek($f, $cursor, SEEK_END);
$char = fgetc($f);

while ($char === "\n" || $char === "\r") {
    fseek($f, $cursor--, SEEK_END);
    $char = fgetc($f);
}


while ($char !== false && $char !== "\n" && $char !== "\r") {

    $line = $char . $line;
    fseek($f, $cursor--, SEEK_END);
    $char = fgetc($f);
}



$result = explode('-',$line);


if(trim($result[2]) == "1") { $status = "Stage 1 loadshedding"; $class = "stage1"; }
elseif(trim($result[2]) == "2") { $status = "Stage 2 loadshedding"; $class = "stage2"; }
elseif(trim($result[2]) == "3") { $status = "Stage 3 loadshedding"; $class = "stage3"; }
else { $status = "No loadshedding"; $class = "stage0"; }

?>

<div class="display <?php echo $class; ?>">

<div class="finder">
	<div class="label">
		Find your loadshedding schedule: 
	</div>

	<div class="selector">
	<!--	<select onchange="location = this.options[this.selectedIndex].value;">
	-->	

<select onChange="window.open(this.options[this.selectedIndex].value,'_top')">
<option>Choose one</option>
		<option value='http://loadshedding.eskom.co.za/'>Eskom customers</option>
		<option value='http://www.iol.co.za/loadshedding-municipalities-1.1869500'>Municipal customers</option>
		</select>
	</div>
</div>

</div>


</body>
</html>

